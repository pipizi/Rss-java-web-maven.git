package com.zhuoyue.po;

import lombok.Data;

@Data
public class Item {

    private String title;
    private String link;
    private String pubdate;
    private String desc;
    private String author;

}

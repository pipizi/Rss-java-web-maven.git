package com.zhuoyue;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.zhuoyue.po.Item;
import com.zhuoyue.po.Student;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class OurParser {

    //psvm
    public static void main1111(String[] args) throws Exception {
        //引入saxreader
        SAXReader saxReader = new SAXReader();
        //读取谁啊
        InputStream resourceAsStream = OurParser.class.getClassLoader().getResourceAsStream("students.xml");
        //解析完毕后会得到一个 document文档 dom对象
        Document document = saxReader.read(resourceAsStream);
        //
        Element rootElement = document.getRootElement();
        System.out.println(rootElement.getName());//zhuoyue
        //父找子
        Element bzr = rootElement.element("bzr");
        String bzrcontent = bzr.getTextTrim();
        System.out.println(bzrcontent);
        //找到所有学生
        Element students = rootElement.element("students");
        List<Element> studentlist = students.elements("student");
        ArrayList<Student> stulist = new ArrayList();
        for (Element stu:studentlist) {

//            Attribute stuno = stu.attribute("stuno");//通过名字获取属性对象
//            System.out.println(stuno.getName()+"="+stuno.getValue());
            String stuno = stu.attributeValue("stuno");
            String name = stu.elementText("name");
            String age = stu.elementText("age");
            String address = stu.elementText("address");
//            System.out.println(stuno+" "+name+" "+age+" "+address);
            Student student = new Student();
            student.setStuno(new Integer(stuno));
            student.setName(name);
            student.setAge(new Integer(age));
            student.setAddress(address);
            stulist.add(student);
        }
        //循环遍历学生
        for (Student student : stulist) {
            System.out.println(student);
        }

    }

}

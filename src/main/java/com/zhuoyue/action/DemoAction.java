package com.zhuoyue.action;

import com.zhuoyue.po.Item;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/show123")
public class DemoAction extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            URL url=new URL("http://www.people.com.cn/rss/culture.xml");
            InputStream inputStream = url.openStream();
            //使用SAXReader需要导入dom4j-full.jar包
            SAXReader saxReader = new SAXReader();
            //生成文档对应实体
            Document document = saxReader.read(inputStream);
            List<Element> items = document.getRootElement().element("channel").elements("item");
            ArrayList<Item> itemList = new ArrayList();
            for(Element el:items){
                String title = el.elementText("title");
                String link = el.elementText("link");
                String pubDate = el.elementText("pubDate");
                String description = el.elementText("description");
                String author = el.elementText("author");
                Item item=new Item();
                item.setTitle(title);
                item.setLink(link);
                item.setAuthor(author);
                item.setPubdate(pubDate);
                item.setDesc(description);
                itemList.add(item);
            }
            for (Item  i:itemList ) {
                System.out.println(i);
            }
        }catch(Exception ex){

        }

    }
}

package com.zhuoyue.action;

import com.zhuoyue.dao.ItemDAO;
import com.zhuoyue.po.Item;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/show")
public class ShowAction extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String url = request.getParameter("url");
        if(url.equals("1")){
            url = "http://www.people.com.cn/rss/politics.xml";
        }else if(url.equals("2")){
            url = "http://www.people.com.cn/rss/world.xml";
        }else if(url.equals("3")){
            url = "http://www.people.com.cn/rss/finance.xml";
        }else if(url.equals("4")){
            url = "http://www.people.com.cn/rss/sports.xml";
        }else if(url.equals("5")){
            url = "http://www.people.com.cn/rss/haixia.xml";
        }else if(url.equals("6")){
            url = "http://www.people.com.cn/rss/edu.xml";
        }else if(url.equals("7")){
            url = "http://www.people.com.cn/rss/bbs.xml";
        }else {
            url = "http://www.people.com.cn/rss/opml_en.xml";
        }
        try {
            List<Item> list = ItemDAO.getItemsByUrl(url);
            request.setAttribute("list", list);
        }catch(Exception ex){}

        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request,response);

    }
}

package com.zhuoyue.action;

import com.zhuoyue.dao.ItemDAO;
import com.zhuoyue.po.Item;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class ShouYeAction extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //跳转到首页
        request.getRequestDispatcher("/WEB-INF/shouye.jsp").forward(request,response);

    }

}

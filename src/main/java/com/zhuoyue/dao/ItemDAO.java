package com.zhuoyue.dao;

import com.zhuoyue.po.Item;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ItemDAO {
    //封装解析细节
    public static List<Item> getItemsByUrl(String strurl) throws Exception {
        //读取网页源代码
        URL url=new URL(strurl);
        //打开网页代码
        InputStream inputStream = url.openStream();
        //解析xml
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read(inputStream);
        List<Element> items = document.getRootElement().element("channel").elements("item");
        ArrayList<Item> itemList = new ArrayList();
        for(Element el:items){
            String title = el.elementText("title");
            String link = el.elementText("link");
            String pubDate = el.elementText("pubDate");
            String description = el.elementText("description");
            String author = el.elementText("author");

            Item item=new Item();
            item.setTitle(title);
            item.setLink(link);
            item.setAuthor(author);
            item.setPubdate(pubDate);
            item.setDesc(description);
            itemList.add(item);
        }
        return itemList;
    }

}

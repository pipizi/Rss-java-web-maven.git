<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 2020/10/13
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <style>
        td{
            font-size:12px;
        }
    </style>
</head>
<body>
    <center>
        <table>
            <c:forEach var="item" items="${list}">
                <tr>
                    <td>
                        <a target="_blank" href="${item.link}">${item.title}</a>
                    </td>
                    <td>${item.pubdate}</td>
                    <td>${item.author}</td>
                </tr>
            </c:forEach>
        </table>
    </center>
</body>
</html>
